# Description: Arithmetic Operators
first_number = float(input("Enter first number: "))
second_number = float(input("Enter second number: "))

# Addition
print("sum: " + str(first_number + second_number))
# Subtraction
print("difference: " + str(first_number -second_number))
# Multiplication
print("product: " + str(first_number * second_number))
# Division
print("quotient: " + str(first_number / second_number))

x = 10
x = x + 3
print(x)
x += 3 # same as x = x + 3
print(x)
x -= 3 # same as x = x - 3
print(x)
x *= 3 # same as x = x * 3
print(x)
x /= 3 # same as x = x / 3
print(x)
x %= 3 # same as x = x % 3
print(x)
x //= 3 # same as x = x // 3
print(x)
x **= 3 # same as x = x ** 3
print(x)