# Description : Return Statement
# Return statement is used to return a value from a function.
# The return statement is used to exit a function and go back to the place from where it was called.

# The return statement can also return a value back to the caller.
def cube(num):
    return num * num * num # return statement
    # print("Code after the return statement will not be executed.")

result = cube(4)
print(result)