# Description : Dictionaries
# Dictionaries are used to store data values in key:value pairs.
# A dictionary is a collection which is ordered*, changeable and does not allow duplicates.
# Dictionaries are written with curly brackets, and have keys and values:

# Key is unique, value can be duplicated
# Values can be of any data type and can be duplicated, whereas keys must be of immutable data type (string, number or tuple with immutable elements) and must be unique.
monthConversions = {
    "Jan": "January",
    "Feb": "February",
    "Mar": "March",
    "Apr": "April",
    "May": "May",
    "Jun": "June",
    "Jul": "July",
    "Aug": "August",
    "Sep": "September",
    "Oct": "October",
    "Nov": "November",
    "Dec": "December",
}

# Print the value of the key "Nov"
print(monthConversions["Nov"]) # November

# Print the value of the key "Dec" using get()
# get() is safer than using square brackets, because it will not throw an error if the key does not exist
print(monthConversions.get("Decasda", "Not a valid key!")) # Not a valid key!
