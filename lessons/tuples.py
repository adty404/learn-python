# Description : Tuples
# Tuple is a collection which is ordered and unchangeable. Allows duplicate members.

# Create tuple
numbers = (1, 2, 3, 3, 4, 5, 5, 5) # tuple

print(numbers[0]) # 1
print(numbers[-1]) # 5
print(numbers.count(5)) # 3

print("=====================================================")

# More on tuples
coordinates = (4, 5)
# coordinates[1] = 10 # TypeError: 'tuple' object does not support item assignment
print(coordinates[0]) # 4
print(coordinates[1]) # 5

print("=====================================================")

# tuples on lists
coordinates = [(4, 5), (6, 7), (80, 34)]
print(coordinates[0]) # (4, 5)

# difference between tuple and list
# list is mutable, tuple is immutable
# list is faster than tuple
# tuple is safer than list
# tuple is used to store related pieces of information
# tuple is used to store data that should not be changed
