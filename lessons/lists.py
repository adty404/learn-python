# Description : Lists
# Lists are a collection of items in a particular order.
# Lists are mutable, meaning the elements inside a list can be changed!
# Lists are defined by having values between square brackets [ ]
# Lists can change, remove, add, and sort items
names = ['John', 'Bob', 'Mosh', 'Sarah', 'Mary']

print(names[0]) # John
print(names[-1]) # Mary
print(names[-2]) # Sarah
print(names[0:3]) # ['John', 'Bob', 'Mosh'] (not including 3
print(names[2:]) # ['Mosh', 'Sarah', 'Mary']
print(names[2:4]) # ['Mosh', 'Sarah'] (not including 4)

names[0] = 'Jon' # replace John with Jon
print(names)

print("=====================================================")

# List Methods
numbers = [1, 2, 3, 4, 5]

# check if an item is in the list
print(1 in numbers) # True

# check the length of the list
print(len(numbers)) # 5

# Add an item to the end of the list
numbers.append(6)
print(numbers)

# Insert an item at a specific position
numbers.insert(0, 0)
print(numbers)

# Remove an item from the end of the list
numbers.pop()
print(numbers)

# Remove an item at a specific position
numbers.pop(0)
print(numbers)

# Remove an item by value
numbers.remove(2)
print(numbers)

# Remove all items
numbers.clear()
print(numbers)
