# Description: Type conversion in Python
birth_year = input("Enter your birth year: ")
# birth_year need to be converted to integer because it is a string
age = 2023 - int(birth_year)

# age need to be converted to string because it is a number
print("Your age is " + str(age) + " years old.")

# Type conversion
int(2.9) # 2
float(2) # 2.0
bool(1) # True
str(100) # "100"

print("=============================================")

# Exercise
first_number = float(input("Enter first number: "))
second_number = float(input("Enter second number: "))

print("sum: " + str(first_number + second_number))
print("difference: " + str(first_number -second_number))
print("product: " + str(first_number * second_number))
print("quotient: " + str(first_number / second_number))
