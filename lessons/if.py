# Description : If Statements
temperature = 8

# If Statements
if temperature > 30:
    print("It's a hot day")
    print("Drink plenty of water")
elif temperature > 20:
    print("It's a nice day")
elif temperature > 10:
    print("It's a bit cold")
else:
    print("It's cold")

print("=============================================")

# Exercise: Weight Converter
weight = float(input("Weight: "))
unit = input("(K)g or (L)bs: ")

if unit.upper() == "K":
    converted = weight / 0.45
    unit = "Kg"
elif unit.upper() == "L":
    converted = weight * 0.45
    unit = "Lbs"

print("Weight in " + unit + ": " + str(converted))