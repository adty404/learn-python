# Description : Range Function
numbers = range(5) # range(0, 5)
numbers = range(5, 10) # range(5, 10), (start, stop)
numbers = range(5, 10, 2) # range(5, 10, 2), (start, stop, step)

for number in numbers:
    print(number)

print("========================================================")

# without numbers variable
for number in range(5):
    print(number)

print("=====")

for number in range(5, 10):
    print(number)

print("=====")

for number in range(5, 10, 2):
    print(number)

print("=====")
