# Description : Functions
# Functions are a way to reuse code. They are a way to group code together to perform a specific task.

# Functions are defined using the def keyword.
def greet_user(name, age):
    print('Hi ' + name + '!')
    print('You are ' + str(age) + ' years old.')
    print('Welcome aboard')

# Functions are called using the function name followed by parentheses.
print("Start")
print("======")
greet_user("Aditya", 23)
print("======")
print("Finish")