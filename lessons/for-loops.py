# Description : For Loops
numbers = [1, 2, 3, 4, 5] # list
for number in numbers:
    print(number)

# example using while (not recommended)
i = 0
while i < len(numbers):
    print(numbers[i])
    i = i + 1