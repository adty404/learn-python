# Description: Variable in Python
name = "Aditya Prasetyo" # String
age = 23 # Integer
is_new_patient = False # Boolean

# String Concatenation
print("Hello World, my name is " + name + " and I am " + str(age) + " years old." + " I am a new patient: " + str(is_new_patient) + ".")

