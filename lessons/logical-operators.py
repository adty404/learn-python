# Description : Logical Operators
price = 25

# Logical Operators
print(price > 10 and price < 30) # True
print(price > 10 or price < 30) # True
print(not price > 10) # False