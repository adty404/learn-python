# Description: Strings in Python
course = 'Python for Beginners'

# String Methods
print(course.upper()) # 'PYTHON FOR BEGINNERS'
print(course.lower()) # 'python for beginners'
print(course.title()) # 'Python For Beginners'
print(course.find('P')) # 0
print(course.find('o')) # 4
print(course.find('Beginners')) # 11
print(course.replace('Beginners', 'Absolute Beginners')) # 'Python for Absolute Beginners'

# in operator
print('Python' in course) # True
