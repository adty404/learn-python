# Description : Sets
# Sets is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Tuple, and Dictionary, all with different qualities and usage.
# Sets is a collection which is unordered, unchangeable and unindexed. No duplicate members.
# Sets are defined by values separated by comma inside braces { }

numbers = {1, 2, 3, 3, 4, 5, 5, 5} # set
first = set(numbers) # set
print(numbers) # {1, 2, 3, 4, 5}

second = {1, 5}
second.add(4)
second.remove(4)
len(second) # 2
print(second) # {1, 4}

print("==========================================")

# union is the items that are in either first or second
print(first | second) # {1, 2, 3, 4, 5}

# intersection is the items that are in both first and second
print(first & second) # {1, 5}

# difference is the items that are in first but not in second
print(first - second) # {2, 3, 4, 5}

# symmetric difference is the items that are in first or second but not both
print(first ^ second) # {2, 3, 4}

