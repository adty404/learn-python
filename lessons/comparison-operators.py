# Description : Comparison Operators
x = 10
y = 3

# Comparison Operators
print(x > y) # Greater than
print(x >= y) # Greater than or equal to
print(x < y) # Less than
print(x <= y) # Less than or equal to
print(x == y) # Equal to
print(x != y) # Not equal to